# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BooksItem(scrapy.Item):
    titre = scrapy.Field()
    prix = scrapy.Field()
    lien = scrapy.Field()
    site = scrapy.Field()
    url = scrapy.Field()
    marque = scrapy.Field()
