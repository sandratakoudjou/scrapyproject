import scrapy
from ..items import BooksItem

class BookspiderSpider(scrapy.Spider):
    name = 'bookspider'
    allowed_domains = ['www.jdsports.fr','www.courir.com']
    start_urls = [
        'https://www.jdsports.fr/homme/chaussures-homme/baskets//marque/nike/',
        'https://www.courir.com/fr/c/marques/nike/?prefn1=genderProduct&prefv1=HOMME'
    ]

    def parse(self, response):
        if 'www.jdsports.fr' in response.url:
            livres = response.xpath('//ul[@id="productListMain"]/li')

            for livre in livres:
                livre_item = BooksItem()

                titre = livre.xpath('.//span[@class="itemTitle"]/a[@data-e2e="product-listing-name"]/text()').get()
                livre_item['titre'] = titre

                prix = livre.xpath('.//span[@class="pri"]/text()').get()
                livre_item['prix'] = prix

                livre_item['site'] = 'JD SPORT'
                livre_item['url'] = livre.xpath('.//span[@class="itemTitle"]/a/@href').get()

                yield livre_item      

            # Naviguer vers la page suivante si elle existe
            #next = response.xpath('//a[@class="btn btn-default pageNav"]/@href').get()
            #if next is not None:
                #next = response.urljoin(next)
                #yield scrapy.Request(url=next, callback=self.parse)
        
        elif 'www.courir.com' in response.url:
            books = response.xpath('//ul[@id="search-result-items"]/li')

            for book in books:

                shoes_item = BooksItem()
                shoes_item['titre'] = 'NIKE '+ book.xpath('.//span[@class="product__name__product js-product-name_product"]/text()').get()
                #shoes['couleur'] = book.xpath('//span[@class="product-color"]/text()').get()
                shoes_item['prix'] =  book.xpath('.//span[@class="default-price js-product__default-price"]/text()').get()
                # shoes['marque'] = book.xpath('//span[@class="product-brand js-product-brand"]/text()').get()
                shoes_item['site'] = 'COURIR'
                shoes_item['url'] = 'courir.com/' + book.xpath('.//a[@class="product__link js-product-link"]/@href').get()
                yield shoes_item

            # Suivant la pagination pour chaque site
            #next_page_url = response.xpath('//div[@class="infinite-scroll-placeholder"]/@data-grid-url').get()
            #if next_page_url:
                #next_page_url = response.urljoin(next_page_url)
                #yield scrapy.Request(url=next_page_url, callback=self.parse)
