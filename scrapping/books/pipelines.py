# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem
import mysql.connector


#class BooksPipeline:
    #def process_item(self, item, spider):
        #return item

#class ConversionPrixPipeline:
    #def process_item(self, item, spider):
        #prix = item['prix']
        #if prix:
            #item['prix'] = item['prix'].replace(",",".")
            #item['prix'] = round(float(prix[1::]), 2)
            #return item
        #else:
            #raise DropItem(f"Le prix {item['titre']} n'a pas une valeur correcte !")

class MysqlPipeline:
    def process_item(self, item, spider):
        mydb = mysql.connector.connect(
            host="database",
            user="root",
            password="root",
            database="book_bd"
        )
        mycursor = mydb.cursor()


        sql = "INSERT INTO jouet2 (nom, prix,site,url) VALUES (%s, %s,%s, %s)"
        val = (item['titre'], item['prix'],item['site'],item['url'])
        mycursor.execute(sql, val)
        mydb.commit()
        return item 
