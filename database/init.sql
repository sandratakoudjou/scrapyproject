CREATE USER IF NOT EXISTS 'root'@'%' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON * . * TO 'root'@'%';
FLUSH PRIVILEGES;
-- Création de la base de données
CREATE DATABASE IF NOT EXISTS book_bd;
USE book_bd;
DROP TABLE IF EXISTS jouet2;
CREATE TABLE IF NOT EXISTS jouet2 (id int AUTO_INCREMENT PRIMARY KEY, nom varchar(255), prix varchar(255), site varchar(900), url varchar(900), etat varchar(255));
CREATE TABLE IF NOT EXISTS products (id int AUTO_INCREMENT PRIMARY KEY, nom varchar(255), prix varchar(255), site varchar(900), url varchar(900), etat varchar(255));
