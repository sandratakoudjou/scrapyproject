<?php
$conn = new mysqli("database", "root", "root", "book_bd");

function getToysFromDatabase() {
    global $conn;

    $query = "SELECT nom, prix, site, url FROM jouet2 ORDER BY nom";
    $result = $conn->query($query);

    // Vérifiez s'il y a des résultats
    if ($result === false) {
        die("Erreur de requête : " . $conn->error);
    }

    // Récupérez les résultats sous forme de tableau associatif
    $toys = array();
    while ($row = $result->fetch_assoc()) {
        $toys[] = $row;
    }

    // Libérez la mémoire du résultat
    $result->free_result();

    return $toys;
}

// Start the session
session_start();
 
 
$servername = "database";
$username = "root";
$password = "root";
 
try {
  $conn = new PDO("mysql:host=$servername;dbname=book_bd", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  #echo "Connexion success";
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
 
#$lien = $_POST['lien_http'];
$toys = getToysFromDatabase();
$verif = 0;
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    if (empty($_POST["lien_http"])) {
        $lienErr = "link is required";
        $verif = 0;
      } else {
        $lienErr = "";
        $lien = test_input($_POST["lien_http"]);
        $verif = 1;
      }
      if ($verif == 1) {
      $requete = "INSERT INTO originale (lien_http) VALUES ('$lien')"; //sans cryptage tu remplace pass3 par pass1
     
        $reponse = $conn->query($requete);
        $id = $conn->lastInsertId();
        echo "
            <script type=\"text/javascript\"> alert('Enregistrement effectué " . $id . "')</script>
            ";
      }
 
}
 
 
 
 
function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
 
?>
 
<!DOCTYPE html>
<html lang="en">
 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
 
  <title>Docker project website</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
 
  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
 
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">
 
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
 
  <!-- Template Main CSS File -->
  <link href="assets/css/style1.css" rel="stylesheet">
 
  <!-- =======================================================
  * Template Name: Regna - v4.10.0
  * Template URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
 
<body>
 
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex justify-content-between align-items-center">
 
      <div id="logo">
        <a href="index.html"><img src="assets/img/docker_logo3.png" alt=""></a>
        <!-- Uncomment below if you prefer to use a text logo -->
        <!--<h1><a href="index.html">Regna</a></h1>-->
      </div>
 
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
         
          <li><a class="nav-link scrollto" href="#facts">Facts</a></li>
         
          <li><a class="nav-link scrollto " href="#portfolio">Sneakers brand & sellers</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->
 
  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
      <h1>WELCOME to DEAL SHOES</h1>
      <h2>Notre projet vise à comparer les prix des sneakers</h2>
      <a href="#about" class="btn-get-started">Get Started</a>
    </div>
  </section><!-- End Hero Section -->
 
  <main id="main">
 
    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">
        <div class="row about-container">
 
          <div class="col-lg-6 content order-lg-1 order-2">
            <h2 class="title">About us</h2>
            <p>
              Par ce site il est possible pour vous de réaliser une comparaison des prix d'article (des sneakers) par rapport à votre recherche personnelle et ce quelque soit la marque (Nike, Adidas, Puma, Converse, New Balance).
            </p>
 
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bi bi-briefcase"></i></div>
              <h4 class="title"><a href="">Service gratuit</a></h4>
              <p class="description">Le service est complètement gratuit et il n'est pas nécessaire d'avoir un compte utilisateur pour bénéficier du service</p>
            </div>
 
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bi bi-database"></i></div>
              <h4 class="title"><a href="">Informations</a></h4>
              <p class="description">En plus de maintenir vos opérations anonymes, nous vous donnons : les noms des vendeurs de votre articles, leurs prix, les liens vous permettant d'y accéder</p>
            </div>
 
            <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
              <div class="icon"><i class="bi bi-key"></i></div>
              <h4 class="title"><a href="">Fiabilité</a></h4>
              <p class="description">Les informations qui vous sont fournies sont d'une fiabilité extrème, il s'agit des données recueillies par notre programme à l'instant T où la demande a été effectuée</p>
            </div>
 
          </div>
 
          <div class="col-lg-6 background order-lg-2 order-1" data-aos="fade-left" data-aos-delay="100"></div>
        </div>
 
      </div>
    </section><!-- End About Section -->
 
    <!-- ======= Facts Section ======= -->
    <section id="facts">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">FACTS</h3>
          <p class="section-description">Quelques faits avant d'effectuer votre de recherche</p>
        </div>
        <div class="row counters">
 
          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="10" data-purecounter-duration="1" class="purecounter"></span>
            <p>Vendeurs</p>
          </div>
 
          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="534" data-purecounter-duration="1" class="purecounter"></span>
            <p>Modèles de chaussures</p>
          </div>
 
          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="45" data-purecounter-duration="1" class="purecounter"></span>
            <p>Heures</p>
          </div>
 
          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="2" data-purecounter-duration="1" class="purecounter"></span>
            <p>Collaborateurs</p>
          </div>
 
        </div>
 
      </div>
    </section><!-- End Facts Section -->
 
    <!-- ======= Services Section
 
 
    <section id="services">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Services</h3>
          <p class="section-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-briefcase"></i></a></div>
              <h4 class="title"><a href="">Lorem Ipsum</a></h4>
              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-card-checklist"></i></a></div>
              <h4 class="title"><a href="">Dolor Sitema</a></h4>
              <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-bar-chart"></i></a></div>
              <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-binoculars"></i></a></div>
              <h4 class="title"><a href="">Magni Dolores</a></h4>
              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-brightness-high"></i></a></div>
              <h4 class="title"><a href="">Nemo Enim</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="zoom-in">
            <div class="box">
              <div class="icon"><a href=""><i class="bi bi-calendar4-week"></i></a></div>
              <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
              <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>
          </div>
        </div>
 
      </div>
    </section>
   
    End Services Section -->
 
    <!-- ======= Call To Action Section ======= -->
    <section id="call-to-action">
      <div class="container">
        <div class="row" data-aos="zoom-in">
            <div class="col-lg-12 text-center">
                <h3 class="cta-title">SNEACKERS LIST</h3>
                <p class="cta-text">Voici la liste des chaussures par ordre de nom :</p>
            </div>

            <div class="col-lg-12">
                <ul class="list-group">
                    <?php
                    // Assumez que $toys est votre tableau d'éléments provenant de la base de données
                    // et qu'il est trié par ordre de nom
                    foreach ($toys as $toy) {
                        echo "<li class=\"list-group-item\">" . $toy['nom'] . " - " . $toy['site'] . " - " . $toy['prix'] . "</li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
      </div>
  </section><!-- End Call To Action Section -->
 
    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Sneakers</h3>
          <p class="section-description">Nous disposons d'un large pannel de sites et de chaussures pour vous rendre la tâche plus simple</p>
        </div>
 
        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">Marche & course</li>
              <li data-filter=".filter-card">Sport</li>
            </ul>
          </div>
        </div>
 
        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/nike1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Nike</h4>
              <p>Air force, Dunk, Air max, ...</p>
              <a href="assets/img/portfolio/nike1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/adidas1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Adidas</h4>
              <p>Niteball, Yeezy, Ozweego, ...</p>
              <a href="assets/img/portfolio/adidas1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/nb4.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>New Balance</h4>
              <p>327, 574, 550, ...</p>
              <a href="assets/img/portfolio/nb4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/converse5.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Converse</h4>
              <p>Chuck Taylor, run star hike, star motion, ...</p>
              <a href="assets/img/portfolio/converse5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/puma3.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Puma</h4>
              <p>Splistream, mirage sport, RS-Z, ...</p>
              <a href="assets/img/portfolio/puma3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/under5.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Under Armor</h4>
              <p>Phantoms, Curry, UA, ...</p>
              <a href="assets/img/portfolio/under5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
 
        </div>
 
      </div>
    </section><!-- End Portfolio Section -->
 
    <!-- ======= Team Section
 
 
    <section id="team">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Team</h3>
          <p class="section-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="pic"><img src="assets/img/team-1.jpg" alt=""></div>
              <h4>Walter White</h4>
              <span>Chief Executive Officer</span>
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>
 
          <div class="col-lg-3 col-md-6">
            <div class="member" data-aos="fade-up" data-aos-delay="200">
              <div class="pic"><img src="assets/img/team-2.jpg" alt=""></div>
              <h4>Sarah Jhinson</h4>
              <span>Product Manager</span>
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>
 
          <div class="col-lg-3 col-md-6">
            <div class="member" data-aos="fade-up" data-aos-delay="300">
              <div class="pic"><img src="assets/img/team-3.jpg" alt=""></div>
              <h4>William Anderson</h4>
              <span>CTO</span>
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>
 
          <div class="col-lg-3 col-md-6">
            <div class="member" data-aos="fade-up" data-aos-delay="400">
              <div class="pic"><img src="assets/img/team-4.jpg" alt=""></div>
              <h4>Amanda Jepson</h4>
              <span>Accountant</span>
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
 
      </div>
    </section>
   
   
    End Team Section -->
 
    <!-- ======= Contact Section
    <section id="contact">
      <div class="container">
        <div class="section-header">
          <h3 class="section-title">Contact</h3>
          <p class="section-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>
      </div>
 
     
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22864.11283411948!2d-73.96468908098944!3d40.630720240038435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbg!4v1540447494452" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
 
      <div class="container mt-5">
        <div class="row justify-content-center">
 
          <div class="col-lg-3 col-md-4">
 
            <div class="info">
              <div>
                <i class="bi bi-geo-alt"></i>
                <p>A108 Adam Street<br>New York, NY 535022</p>
              </div>
 
              <div>
                <i class="bi bi-envelope"></i>
                <p>info@example.com</p>
              </div>
 
              <div>
                <i class="bi bi-phone"></i>
                <p>+1 5589 55488 55s</p>
              </div>
            </div>
 
            <div class="social-links">
              <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
              <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
              <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
              <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
              <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
            </div>
 
          </div>
 
          <div class="col-lg-5 col-md-8">
            <div class="form">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="form-group mt-3">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
                <div class="form-group mt-3">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                </div>
                <div class="form-group mt-3">
                  <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                </div>
                <div class="my-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit">Send Message</button></div>
              </form>
            </div>
          </div>
 
        </div>
 
      </div>
    </section>
 
   
    End Contact Section -->
 
  </main><!-- End #main -->
 
  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
 
      </div>
    </div>
 
    <div class="container">
      <div class="copyright">
        Copyright &copy; 2023 <strong>Valentin & Rayann</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Regna
      -->
       
        <div class="social-links">
          We can see the git repository <a href="">Valentin & Rayann</a>
          <a href="#" class="github"><i class="bi bi-github"></i></a>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->
 
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
 
  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
 
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
 
</body>
 
</html>